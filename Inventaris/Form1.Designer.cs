﻿namespace Inventaris
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_type_code = new System.Windows.Forms.TextBox();
            this.txt_type_nama = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_type_delete = new System.Windows.Forms.Button();
            this.btn_type_update = new System.Windows.Forms.Button();
            this.btn_type_simpan = new System.Windows.Forms.Button();
            this.btn_type_clear = new System.Windows.Forms.Button();
            this.table_type = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transactiontypesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transaction_type_data_table = new Inventaris.transaction_type_data_table();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_master_code = new System.Windows.Forms.TextBox();
            this.txt_master_nama = new System.Windows.Forms.TextBox();
            this.txt_master_qty = new System.Windows.Forms.TextBox();
            this.txt_master_keterangan = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_master_simpan = new System.Windows.Forms.Button();
            this.btn_master_update = new System.Windows.Forms.Button();
            this.btn_master_delete = new System.Windows.Forms.Button();
            this.btn_master_clear = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.table_master = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_transaksi_type = new System.Windows.Forms.TextBox();
            this.txt_transaksi_keterangan = new System.Windows.Forms.TextBox();
            this.txt_transaksi_code = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_transaksi_simpan = new System.Windows.Forms.Button();
            this.btn_transaksi_update = new System.Windows.Forms.Button();
            this.btn_transaksi_delete = new System.Windows.Forms.Button();
            this.btn_transaksi_clear = new System.Windows.Forms.Button();
            this.table_transaksi = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transcodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transdateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarksDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.transactionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pemvisDataSet = new Inventaris.pemvisDataSet();
            this.transactiondetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.trans_detail_table = new Inventaris.trans_detail_table();
            this.transaction_typesTableAdapter = new Inventaris.transaction_type_data_tableTableAdapters.transaction_typesTableAdapter();
            this.transaction_data_table = new Inventaris.transaction_data_table();
            this.transactiondatatableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transactionsTableAdapter = new Inventaris.pemvisDataSetTableAdapters.transactionsTableAdapter();
            this.transaction_detailsTableAdapter = new Inventaris.trans_detail_tableTableAdapters.transaction_detailsTableAdapter();
            this.items_table = new Inventaris.items_table();
            this.itemsBindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.itemsTableAdapter = new Inventaris.items_tableTableAdapters.itemsTableAdapter();
            this.idDataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityonhandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.remarksDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.itemsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.itemsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiontypesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transaction_type_data_table)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_master)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.table_transaksi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pemvisDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiondetailsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trans_detail_table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transaction_data_table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiondatatableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.items_table)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(634, 480);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.table_type);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(626, 454);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Type Transaksi";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(614, 196);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Type Transaksi";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.61842F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.38158F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt_type_code, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt_type_nama, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(608, 102);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama";
            // 
            // txt_type_code
            // 
            this.txt_type_code.Location = new System.Drawing.Point(176, 3);
            this.txt_type_code.Name = "txt_type_code";
            this.txt_type_code.Size = new System.Drawing.Size(425, 20);
            this.txt_type_code.TabIndex = 3;
            // 
            // txt_type_nama
            // 
            this.txt_type_nama.Location = new System.Drawing.Point(176, 30);
            this.txt_type_nama.Name = "txt_type_nama";
            this.txt_type_nama.Size = new System.Drawing.Size(425, 20);
            this.txt_type_nama.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_type_delete);
            this.panel1.Controls.Add(this.btn_type_update);
            this.panel1.Controls.Add(this.btn_type_simpan);
            this.panel1.Controls.Add(this.btn_type_clear);
            this.panel1.Location = new System.Drawing.Point(176, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(425, 37);
            this.panel1.TabIndex = 5;
            // 
            // btn_type_delete
            // 
            this.btn_type_delete.Location = new System.Drawing.Point(104, 2);
            this.btn_type_delete.Name = "btn_type_delete";
            this.btn_type_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_type_delete.TabIndex = 3;
            this.btn_type_delete.Text = "Delete";
            this.btn_type_delete.UseVisualStyleBackColor = true;
            this.btn_type_delete.Click += new System.EventHandler(this.btn_type_delete_Click);
            // 
            // btn_type_update
            // 
            this.btn_type_update.Location = new System.Drawing.Point(185, 3);
            this.btn_type_update.Name = "btn_type_update";
            this.btn_type_update.Size = new System.Drawing.Size(75, 23);
            this.btn_type_update.TabIndex = 2;
            this.btn_type_update.Text = "Update";
            this.btn_type_update.UseVisualStyleBackColor = true;
            this.btn_type_update.Click += new System.EventHandler(this.btn_type_update_Click);
            // 
            // btn_type_simpan
            // 
            this.btn_type_simpan.Location = new System.Drawing.Point(266, 3);
            this.btn_type_simpan.Name = "btn_type_simpan";
            this.btn_type_simpan.Size = new System.Drawing.Size(75, 23);
            this.btn_type_simpan.TabIndex = 1;
            this.btn_type_simpan.Text = "Simpan";
            this.btn_type_simpan.UseVisualStyleBackColor = true;
            this.btn_type_simpan.Click += new System.EventHandler(this.btn_type_simpan_Click);
            // 
            // btn_type_clear
            // 
            this.btn_type_clear.Location = new System.Drawing.Point(347, 3);
            this.btn_type_clear.Name = "btn_type_clear";
            this.btn_type_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_type_clear.TabIndex = 0;
            this.btn_type_clear.Text = "Clear";
            this.btn_type_clear.UseVisualStyleBackColor = true;
            this.btn_type_clear.Click += new System.EventHandler(this.btn_type_clear_Click);
            // 
            // table_type
            // 
            this.table_type.AutoGenerateColumns = false;
            this.table_type.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_type.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn});
            this.table_type.DataSource = this.transactiontypesBindingSource;
            this.table_type.Location = new System.Drawing.Point(6, 208);
            this.table_type.Name = "table_type";
            this.table_type.Size = new System.Drawing.Size(617, 240);
            this.table_type.TabIndex = 0;
            this.table_type.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_type_CellClick);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "code";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            // 
            // transactiontypesBindingSource
            // 
            this.transactiontypesBindingSource.DataMember = "transaction_types";
            this.transactiontypesBindingSource.DataSource = this.transaction_type_data_table;
            // 
            // transaction_type_data_table
            // 
            this.transaction_type_data_table.DataSetName = "transaction_type_data_table";
            this.transaction_type_data_table.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.table_master);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(626, 454);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Master Inventaris";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel2);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(614, 220);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Master Barang";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.26246F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.73754F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.txt_master_code, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txt_master_nama, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txt_master_qty, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txt_master_keterangan, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(602, 143);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Nama";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Qty";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "keterangan";
            // 
            // txt_master_code
            // 
            this.txt_master_code.Location = new System.Drawing.Point(131, 3);
            this.txt_master_code.Name = "txt_master_code";
            this.txt_master_code.Size = new System.Drawing.Size(468, 20);
            this.txt_master_code.TabIndex = 4;
            // 
            // txt_master_nama
            // 
            this.txt_master_nama.Location = new System.Drawing.Point(131, 29);
            this.txt_master_nama.Name = "txt_master_nama";
            this.txt_master_nama.Size = new System.Drawing.Size(468, 20);
            this.txt_master_nama.TabIndex = 5;
            // 
            // txt_master_qty
            // 
            this.txt_master_qty.Location = new System.Drawing.Point(131, 55);
            this.txt_master_qty.Name = "txt_master_qty";
            this.txt_master_qty.Size = new System.Drawing.Size(468, 20);
            this.txt_master_qty.TabIndex = 6;
            // 
            // txt_master_keterangan
            // 
            this.txt_master_keterangan.Location = new System.Drawing.Point(131, 81);
            this.txt_master_keterangan.Name = "txt_master_keterangan";
            this.txt_master_keterangan.Size = new System.Drawing.Size(468, 20);
            this.txt_master_keterangan.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btn_master_simpan);
            this.panel2.Controls.Add(this.btn_master_update);
            this.panel2.Controls.Add(this.btn_master_delete);
            this.panel2.Controls.Add(this.btn_master_clear);
            this.panel2.Location = new System.Drawing.Point(131, 108);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(468, 32);
            this.panel2.TabIndex = 8;
            // 
            // btn_master_simpan
            // 
            this.btn_master_simpan.Location = new System.Drawing.Point(147, 6);
            this.btn_master_simpan.Name = "btn_master_simpan";
            this.btn_master_simpan.Size = new System.Drawing.Size(75, 23);
            this.btn_master_simpan.TabIndex = 3;
            this.btn_master_simpan.Text = "Simpan";
            this.btn_master_simpan.UseVisualStyleBackColor = true;
            this.btn_master_simpan.Click += new System.EventHandler(this.btn_master_simpan_Click);
            // 
            // btn_master_update
            // 
            this.btn_master_update.Location = new System.Drawing.Point(228, 6);
            this.btn_master_update.Name = "btn_master_update";
            this.btn_master_update.Size = new System.Drawing.Size(75, 23);
            this.btn_master_update.TabIndex = 2;
            this.btn_master_update.Text = "Update";
            this.btn_master_update.UseVisualStyleBackColor = true;
            this.btn_master_update.Click += new System.EventHandler(this.btn_master_update_Click);
            // 
            // btn_master_delete
            // 
            this.btn_master_delete.Location = new System.Drawing.Point(309, 6);
            this.btn_master_delete.Name = "btn_master_delete";
            this.btn_master_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_master_delete.TabIndex = 1;
            this.btn_master_delete.Text = "Delete";
            this.btn_master_delete.UseVisualStyleBackColor = true;
            this.btn_master_delete.Click += new System.EventHandler(this.btn_master_delete_Click);
            // 
            // btn_master_clear
            // 
            this.btn_master_clear.Location = new System.Drawing.Point(390, 5);
            this.btn_master_clear.Name = "btn_master_clear";
            this.btn_master_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_master_clear.TabIndex = 0;
            this.btn_master_clear.Text = "Clear";
            this.btn_master_clear.UseVisualStyleBackColor = true;
            this.btn_master_clear.Click += new System.EventHandler(this.btn_master_clear_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Code";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // table_master
            // 
            this.table_master.AutoGenerateColumns = false;
            this.table_master.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_master.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn4,
            this.codeDataGridViewTextBoxColumn2,
            this.nameDataGridViewTextBoxColumn2,
            this.quantityonhandDataGridViewTextBoxColumn,
            this.remarksDataGridViewTextBoxColumn2});
            this.table_master.DataSource = this.itemsBindingSource2;
            this.table_master.Location = new System.Drawing.Point(6, 232);
            this.table_master.Name = "table_master";
            this.table_master.Size = new System.Drawing.Size(614, 216);
            this.table_master.TabIndex = 0;
            this.table_master.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_master_CellClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.table_transaksi);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(626, 454);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Transaksi Inventaris";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tableLayoutPanel3);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(614, 190);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.39535F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.60465F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txt_transaksi_type, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txt_transaksi_keterangan, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.txt_transaksi_code, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 1, 4);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(602, 165);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Type";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Code";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Keterangan";
            // 
            // txt_transaksi_type
            // 
            this.txt_transaksi_type.Location = new System.Drawing.Point(192, 3);
            this.txt_transaksi_type.Name = "txt_transaksi_type";
            this.txt_transaksi_type.Size = new System.Drawing.Size(100, 20);
            this.txt_transaksi_type.TabIndex = 4;
            // 
            // txt_transaksi_keterangan
            // 
            this.txt_transaksi_keterangan.Location = new System.Drawing.Point(192, 57);
            this.txt_transaksi_keterangan.Name = "txt_transaksi_keterangan";
            this.txt_transaksi_keterangan.Size = new System.Drawing.Size(407, 20);
            this.txt_transaksi_keterangan.TabIndex = 5;
            // 
            // txt_transaksi_code
            // 
            this.txt_transaksi_code.Location = new System.Drawing.Point(192, 30);
            this.txt_transaksi_code.Name = "txt_transaksi_code";
            this.txt_transaksi_code.Size = new System.Drawing.Size(100, 20);
            this.txt_transaksi_code.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_transaksi_simpan);
            this.panel3.Controls.Add(this.btn_transaksi_update);
            this.panel3.Controls.Add(this.btn_transaksi_delete);
            this.panel3.Controls.Add(this.btn_transaksi_clear);
            this.panel3.Location = new System.Drawing.Point(192, 111);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(407, 51);
            this.panel3.TabIndex = 8;
            // 
            // btn_transaksi_simpan
            // 
            this.btn_transaksi_simpan.Location = new System.Drawing.Point(89, 13);
            this.btn_transaksi_simpan.Name = "btn_transaksi_simpan";
            this.btn_transaksi_simpan.Size = new System.Drawing.Size(75, 23);
            this.btn_transaksi_simpan.TabIndex = 3;
            this.btn_transaksi_simpan.Text = "Simpan";
            this.btn_transaksi_simpan.UseVisualStyleBackColor = true;
            this.btn_transaksi_simpan.Click += new System.EventHandler(this.btn_transaksi_simpan_Click);
            // 
            // btn_transaksi_update
            // 
            this.btn_transaksi_update.Location = new System.Drawing.Point(170, 14);
            this.btn_transaksi_update.Name = "btn_transaksi_update";
            this.btn_transaksi_update.Size = new System.Drawing.Size(75, 23);
            this.btn_transaksi_update.TabIndex = 2;
            this.btn_transaksi_update.Text = "Update";
            this.btn_transaksi_update.UseVisualStyleBackColor = true;
            this.btn_transaksi_update.Click += new System.EventHandler(this.btn_transaksi_update_Click);
            // 
            // btn_transaksi_delete
            // 
            this.btn_transaksi_delete.Location = new System.Drawing.Point(251, 14);
            this.btn_transaksi_delete.Name = "btn_transaksi_delete";
            this.btn_transaksi_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_transaksi_delete.TabIndex = 1;
            this.btn_transaksi_delete.Text = "Delete";
            this.btn_transaksi_delete.UseVisualStyleBackColor = true;
            this.btn_transaksi_delete.Click += new System.EventHandler(this.btn_transaksi_delete_Click);
            // 
            // btn_transaksi_clear
            // 
            this.btn_transaksi_clear.Location = new System.Drawing.Point(332, 14);
            this.btn_transaksi_clear.Name = "btn_transaksi_clear";
            this.btn_transaksi_clear.Size = new System.Drawing.Size(75, 23);
            this.btn_transaksi_clear.TabIndex = 0;
            this.btn_transaksi_clear.Text = "Clear";
            this.btn_transaksi_clear.UseVisualStyleBackColor = true;
            this.btn_transaksi_clear.Click += new System.EventHandler(this.btn_transaksi_clear_Click);
            // 
            // table_transaksi
            // 
            this.table_transaksi.AutoGenerateColumns = false;
            this.table_transaksi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_transaksi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn2,
            this.typeidDataGridViewTextBoxColumn,
            this.transcodeDataGridViewTextBoxColumn,
            this.transdateDataGridViewTextBoxColumn,
            this.remarksDataGridViewTextBoxColumn});
            this.table_transaksi.DataSource = this.transactionsBindingSource;
            this.table_transaksi.Location = new System.Drawing.Point(6, 202);
            this.table_transaksi.Name = "table_transaksi";
            this.table_transaksi.Size = new System.Drawing.Size(614, 246);
            this.table_transaksi.TabIndex = 0;
            this.table_transaksi.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_transaksi_CellClick);
            this.table_transaksi.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.table_transaksi_CellDoubleClick);
            // 
            // idDataGridViewTextBoxColumn2
            // 
            this.idDataGridViewTextBoxColumn2.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn2.HeaderText = "id";
            this.idDataGridViewTextBoxColumn2.Name = "idDataGridViewTextBoxColumn2";
            this.idDataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // typeidDataGridViewTextBoxColumn
            // 
            this.typeidDataGridViewTextBoxColumn.DataPropertyName = "type_id";
            this.typeidDataGridViewTextBoxColumn.HeaderText = "type_id";
            this.typeidDataGridViewTextBoxColumn.Name = "typeidDataGridViewTextBoxColumn";
            // 
            // transcodeDataGridViewTextBoxColumn
            // 
            this.transcodeDataGridViewTextBoxColumn.DataPropertyName = "trans_code";
            this.transcodeDataGridViewTextBoxColumn.HeaderText = "trans_code";
            this.transcodeDataGridViewTextBoxColumn.Name = "transcodeDataGridViewTextBoxColumn";
            // 
            // transdateDataGridViewTextBoxColumn
            // 
            this.transdateDataGridViewTextBoxColumn.DataPropertyName = "trans_date";
            this.transdateDataGridViewTextBoxColumn.HeaderText = "trans_date";
            this.transdateDataGridViewTextBoxColumn.Name = "transdateDataGridViewTextBoxColumn";
            // 
            // remarksDataGridViewTextBoxColumn
            // 
            this.remarksDataGridViewTextBoxColumn.DataPropertyName = "remarks";
            this.remarksDataGridViewTextBoxColumn.HeaderText = "remarks";
            this.remarksDataGridViewTextBoxColumn.Name = "remarksDataGridViewTextBoxColumn";
            // 
            // transactionsBindingSource
            // 
            this.transactionsBindingSource.DataMember = "transactions";
            this.transactionsBindingSource.DataSource = this.pemvisDataSet;
            // 
            // pemvisDataSet
            // 
            this.pemvisDataSet.DataSetName = "pemvisDataSet";
            this.pemvisDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // transactiondetailsBindingSource
            // 
            this.transactiondetailsBindingSource.DataMember = "transaction_details";
            this.transactiondetailsBindingSource.DataSource = this.trans_detail_table;
            // 
            // trans_detail_table
            // 
            this.trans_detail_table.DataSetName = "trans_detail_table";
            this.trans_detail_table.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // transaction_typesTableAdapter
            // 
            this.transaction_typesTableAdapter.ClearBeforeFill = true;
            // 
            // transaction_data_table
            // 
            this.transaction_data_table.DataSetName = "transaction_data_table";
            this.transaction_data_table.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // transactiondatatableBindingSource
            // 
            this.transactiondatatableBindingSource.DataSource = this.transaction_data_table;
            this.transactiondatatableBindingSource.Position = 0;
            // 
            // transactionsTableAdapter
            // 
            this.transactionsTableAdapter.ClearBeforeFill = true;
            // 
            // transaction_detailsTableAdapter
            // 
            this.transaction_detailsTableAdapter.ClearBeforeFill = true;
            // 
            // items_table
            // 
            this.items_table.DataSetName = "items_table";
            this.items_table.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // itemsBindingSource2
            // 
            this.itemsBindingSource2.DataMember = "items";
            this.itemsBindingSource2.DataSource = this.items_table;
            // 
            // itemsTableAdapter
            // 
            this.itemsTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn4
            // 
            this.idDataGridViewTextBoxColumn4.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn4.HeaderText = "id";
            this.idDataGridViewTextBoxColumn4.Name = "idDataGridViewTextBoxColumn4";
            this.idDataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // codeDataGridViewTextBoxColumn2
            // 
            this.codeDataGridViewTextBoxColumn2.DataPropertyName = "code";
            this.codeDataGridViewTextBoxColumn2.HeaderText = "code";
            this.codeDataGridViewTextBoxColumn2.Name = "codeDataGridViewTextBoxColumn2";
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "name";
            this.nameDataGridViewTextBoxColumn2.HeaderText = "name";
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            // 
            // quantityonhandDataGridViewTextBoxColumn
            // 
            this.quantityonhandDataGridViewTextBoxColumn.DataPropertyName = "quantity_on_hand";
            this.quantityonhandDataGridViewTextBoxColumn.HeaderText = "quantity_on_hand";
            this.quantityonhandDataGridViewTextBoxColumn.Name = "quantityonhandDataGridViewTextBoxColumn";
            // 
            // remarksDataGridViewTextBoxColumn2
            // 
            this.remarksDataGridViewTextBoxColumn2.DataPropertyName = "remarks";
            this.remarksDataGridViewTextBoxColumn2.HeaderText = "remarks";
            this.remarksDataGridViewTextBoxColumn2.Name = "remarksDataGridViewTextBoxColumn2";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(562, 499);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "refresh";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 529);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aplikasi Inventaris";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiontypesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transaction_type_data_table)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_master)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.table_transaksi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pemvisDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiondetailsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trans_detail_table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transaction_data_table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transactiondatatableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.items_table)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView table_type;
        private transaction_type_data_table transaction_type_data_table;
        private System.Windows.Forms.BindingSource transactiontypesBindingSource;
        private transaction_type_data_tableTableAdapters.transaction_typesTableAdapter transaction_typesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView table_master;
        private System.Windows.Forms.BindingSource itemsBindingSource;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_type_code;
        private System.Windows.Forms.TextBox txt_type_nama;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_type_delete;
        private System.Windows.Forms.Button btn_type_update;
        private System.Windows.Forms.Button btn_type_simpan;
        private System.Windows.Forms.Button btn_type_clear;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.BindingSource itemsBindingSource1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_master_code;
        private System.Windows.Forms.TextBox txt_master_nama;
        private System.Windows.Forms.TextBox txt_master_qty;
        private System.Windows.Forms.TextBox txt_master_keterangan;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_master_simpan;
        private System.Windows.Forms.Button btn_master_update;
        private System.Windows.Forms.Button btn_master_delete;
        private System.Windows.Forms.Button btn_master_clear;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView table_transaksi;
        private System.Windows.Forms.BindingSource transactiondatatableBindingSource;
        private transaction_data_table transaction_data_table;
        private pemvisDataSet pemvisDataSet;
        private System.Windows.Forms.BindingSource transactionsBindingSource;
        private pemvisDataSetTableAdapters.transactionsTableAdapter transactionsTableAdapter;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_transaksi_type;
        private System.Windows.Forms.TextBox txt_transaksi_keterangan;
        private System.Windows.Forms.TextBox txt_transaksi_code;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_transaksi_simpan;
        private System.Windows.Forms.Button btn_transaksi_update;
        private System.Windows.Forms.Button btn_transaksi_delete;
        private System.Windows.Forms.Button btn_transaksi_clear;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeidDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transcodeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn transdateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarksDataGridViewTextBoxColumn;
        private trans_detail_table trans_detail_table;
        private System.Windows.Forms.BindingSource transactiondetailsBindingSource;
        private trans_detail_tableTableAdapters.transaction_detailsTableAdapter transaction_detailsTableAdapter;
        private items_table items_table;
        private System.Windows.Forms.BindingSource itemsBindingSource2;
        private items_tableTableAdapters.itemsTableAdapter itemsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityonhandDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn remarksDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Button button1;
    }
}

