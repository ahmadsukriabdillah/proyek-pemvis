﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventaris
{
    public partial class DetaiTransaction : Form
    {
        private Int32 selected_transaction;
        private Int32 selected_trans_id;

        public DetaiTransaction()
        {
            InitializeComponent();

            InventoryEntity inv = new InventoryEntity();
            var dataSourceItem = inv.items.ToList();
            dataSourceItem.ForEach(x =>
            {
                cbox_items_id.Items.Add(new AdapterCBOX(x.id, x.name));
            });
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AdapterCBOX res = cbox_items_id.SelectedItem as AdapterCBOX;
            var contex = new InventoryEntity();
            
            InventoryEntity inv = new InventoryEntity();
            var dataSourceItem = inv.items.Where(x => res.Id == x.id).ToList();
            var found = dataSourceItem.First();
            Int32 hasil = Convert.ToInt32(found.quantity_on_hand) + Convert.ToInt32(txt_qty.Text);
            string query2 = "UPDATE dbo.items SET quantity_on_hand = " + hasil  + " WHERE id=" + Convert.ToInt32(found.id);
            contex.Database.ExecuteSqlCommand(query2);

            string query = "INSERT INTO dbo.transaction_details (trans_id, item_id,quantity,remarks) VALUES (" + Convert.ToInt32(txt_trans_id.Text) + "," + res.Id + ",'" + Convert.ToInt32(txt_qty.Text) + "','" + txt_keterangan.Text + "')";
            contex.Database.ExecuteSqlCommand(query);
            refresh_items();
            clear_transaction();
            selected_trans_id = 0;
            MessageBox.Show("Simpan data berhasil!!");
        }

        private void clear_transaction()
        {
            txt_keterangan.Text = "";
            txt_qty.Text = "";
            cbox_items_id.SelectedIndex = 0;
        }

        internal void setTransaction(string selected_transaction)
        {
            this.selected_transaction = Convert.ToInt32(selected_transaction);
            txt_trans_id.Text = selected_transaction;
            refresh_items();
        }

        private void refresh_items()
        {
            InventoryEntity inv = new InventoryEntity();
            var item = inv.transaction_details.Where(x => x.trans_id == selected_transaction).ToList();
            table_detail_transaction.DataSource = item;
        }

        private class AdapterCBOX
        {
            public AdapterCBOX(int idTemp, string nameTemp)
            {
                Id = idTemp; name = nameTemp;
            }
            public int Id { get; set; }
            public string name { get; set; }
            public override string ToString()
            {
                return name;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(selected_trans_id == 0)
            {
                MessageBox.Show("Pilih data yang akan di hapus terlebih dahulu");
                return;
            }
            var contex = new InventoryEntity();
            string query = "DELETE FROM DBO.transaction_details WHERE id = " + selected_trans_id;
            contex.Database.ExecuteSqlCommand(query);
            refresh_items();
            clear_transaction();
            selected_trans_id = 0;
            MessageBox.Show("Hapus data berhasil!!");
        }

        private void table_detail_transaction_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.table_detail_transaction.Rows[e.RowIndex];
                txt_keterangan.Text = row.Cells[4].Value.ToString();
                txt_qty.Text = row.Cells[3].Value.ToString();
            }
        }
    }
}
