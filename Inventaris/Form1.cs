﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Inventaris
{
    public partial class Form1 : Form
    {
        public string selected_transaction_type { get; private set; }
        public string selected_master { get; private set; }
        public string selected_transaction { get; private set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'items_table.items' table. You can move, or remove it, as needed.
            //this.itemsTableAdapter.Fill(this.items_table.items);
            // TODO: This line of code loads data into the 'trans_detail_table.transaction_details' table. You can move, or remove it, as needed.
            //this.transaction_detailsTableAdapter.Fill(this.trans_detail_table.transaction_details);
            // TODO: This line of code loads data into the 'pemvisDataSet.transactions' table. You can move, or remove it, as needed.
            //this.transactionsTableAdapter.Fill(this.pemvisDataSet.transactions);
            // TODO: This line of code loads data into the 'transaction_type_data_table.transaction_types' table. You can move, or remove it, as needed.
            //this.transaction_typesTableAdapter.Fill(this.transaction_type_data_table.transaction_types);
            this.refresh_items();
            this.refresh_transaction();
            this.refresh_type();

        }


        private void btn_type_update_Click(object sender, EventArgs e)
        {
            if (selected_transaction_type == null)
            {
                MessageBox.Show("Pilih data yang akan di edit terlebih dahulu");
                return;
            }
            var contex = new InventoryEntity();
            string query = "UPDATE dbo.transaction_types SET code='" + txt_type_code.Text + "', name='" + txt_type_nama.Text + "' WHERE id=" + selected_transaction_type;
            contex.Database.ExecuteSqlCommand(query);
            clear_type();
            refresh_type();
            MessageBox.Show("Update data berhasil!!");
        }

        private void refresh_items()
        {
            InventoryEntity inv = new InventoryEntity();
            var item = inv.items.ToList();
            table_master.DataSource = item;
        }
        private void refresh_transaction()
        {
            InventoryEntity inv = new InventoryEntity();
            var item = inv.transactions.ToList();
            table_transaksi.DataSource = item;
        }
        private void refresh_type()
        {
            InventoryEntity inv = new InventoryEntity();
            var item = inv.transaction_types.ToList();
            table_type.DataSource = item;
        }

        private void btn_master_simpan_Click(object sender, EventArgs e)
        {
            var contex = new InventoryEntity();
            string query = "INSERT INTO dbo.items (code, name,quantity_on_hand,remarks) VALUES ('" + txt_master_code.Text + "','" + txt_master_nama.Text + "','" + txt_master_qty.Text + "','" + txt_master_keterangan.Text + "')";
            contex.Database.ExecuteSqlCommand(query);
            refresh_items();
            clear_items();
            selected_master = null;
            MessageBox.Show("Simpan data berhasil!!");
        }

        private void btn_type_simpan_Click(object sender, EventArgs e)
        {
            var contex = new InventoryEntity();
            string query = "INSERT INTO dbo.transaction_types (code, name) VALUES ('" + txt_type_code.Text + "','" + txt_type_nama.Text + "')";
            contex.Database.ExecuteSqlCommand(query);
            refresh_type();
            clear_type();
            selected_transaction_type = null;
            MessageBox.Show("Simpan data berhasil!!");
        }

        private void clear_type()
        {
            txt_type_code.Text = "";
            txt_type_nama.Text = "";
            selected_transaction_type = null;

        }

        private void btn_type_clear_Click(object sender, EventArgs e)
        {
            clear_type();
            selected_transaction_type = null;

        }

        private void btn_type_delete_Click(object sender, EventArgs e)
        {
            if(selected_transaction_type == null){
                MessageBox.Show("Pilih data yang akan di hapus terlebih dahulu");
                return;
            }
            var contex = new InventoryEntity();
            string query = "DELETE FROM DBO.transaction_types WHERE id = " + selected_transaction_type;
            contex.Database.ExecuteSqlCommand(query);
            clear_type();
            refresh_type();
            selected_transaction_type = null;

            MessageBox.Show("Hapus data berhasil!!");
        }

        private void table_type_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.table_type.Rows[e.RowIndex];
                selected_transaction_type = row.Cells[0].Value.ToString();
                txt_type_code.Text = row.Cells[1].Value.ToString();
                txt_type_nama.Text = row.Cells[2].Value.ToString();
            }
        }

        private void btn_master_delete_Click(object sender, EventArgs e)
        {
            if (selected_transaction_type == null)
            {
                MessageBox.Show("Pilih data yang akan di hapus terlebih dahulu");
                return;
            }
            var contex = new InventoryEntity();
            string query = "DELETE FROM DBO.items WHERE id = " + selected_master;
            contex.Database.ExecuteSqlCommand(query);
            refresh_items();
            clear_items();
            selected_master = null;
            MessageBox.Show("Hapus data berhasil!!");
        }

        private void clear_items()
        {
            txt_master_code.Text = "";
            txt_master_nama.Text = "";
            txt_master_qty.Text = "";
            txt_master_keterangan.Text = "";
        }

        private void btn_master_clear_Click(object sender, EventArgs e)
        {
            clear_items();
        }

        private void btn_master_update_Click(object sender, EventArgs e)
        {
            if (selected_master == null)
            {
                MessageBox.Show("Pilih data yang akan di edit terlebih dahulu");
                return;
            }

            var contex = new InventoryEntity();
            string query = "UPDATE dbo.items SET code='" + txt_master_code.Text + "', name='" + txt_master_nama.Text + "', quantity_on_hand = " + Convert.ToInt32(txt_master_qty.Text) + ", remarks='" + txt_master_keterangan.Text + "' WHERE id=" + Convert.ToInt32(selected_master);
            contex.Database.ExecuteSqlCommand(query);
            clear_items();
            refresh_items();
            MessageBox.Show("Update data berhasil!!");
        }

        private void table_master_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.table_master.Rows[e.RowIndex];
                selected_master = row.Cells[0].Value.ToString();
                txt_master_code.Text = row.Cells[1].Value.ToString();
                txt_master_nama.Text = row.Cells[2].Value.ToString();
                txt_master_qty.Text = row.Cells[3].Value.ToString();
                txt_master_keterangan.Text = row.Cells[4].Value.ToString();

            }
        }

        private void btn_transaksi_simpan_Click(object sender, EventArgs e)
        {
            var contex = new InventoryEntity();
            DateTime localDate = DateTime.Now;
            string date = localDate.ToString();
            string query = "INSERT INTO dbo.transactions(type_id, trans_code,trans_date,remarks) VALUES (" + Convert.ToInt32(txt_transaksi_type.Text) + ",'" + txt_transaksi_code.Text + "','" + date + "','" + txt_transaksi_keterangan.Text + "')";
            contex.Database.ExecuteSqlCommand(query);
            refresh_transaction();
            clear_transaction();
            selected_transaction = null;
            MessageBox.Show("Simpan data berhasil!!");
        }

        private void clear_transaction()
        {
            txt_transaksi_code.Text = "";
            txt_transaksi_keterangan.Text = "";
            txt_transaksi_type.Text = "";
            selected_transaction = null;
        }

        private void table_transaksi_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.table_transaksi.Rows[e.RowIndex];
                selected_transaction = row.Cells[0].Value.ToString();
                txt_transaksi_type.Text = row.Cells[1].Value.ToString();
                txt_transaksi_code.Text = row.Cells[2].Value.ToString();
                txt_transaksi_keterangan.Text = row.Cells[3].Value.ToString();
            }
        }

        private void btn_transaksi_clear_Click(object sender, EventArgs e)
        {
            clear_transaction();
        }

        private void btn_transaksi_delete_Click(object sender, EventArgs e)
        {
            if (selected_transaction == null)
            {
                MessageBox.Show("Pilih data yang akan di hapus terlebih dahulu");
                return;
            }
            var contex = new InventoryEntity();
            string query = "DELETE FROM DBO.transaction WHERE id = " + selected_transaction;
            contex.Database.ExecuteSqlCommand(query);
            clear_transaction();
            refresh_transaction();
            selected_transaction = null;

            MessageBox.Show("Hapus data berhasil!!");
        }

        private void btn_transaksi_update_Click(object sender, EventArgs e)
        {
            if (selected_transaction == null)
            {
                MessageBox.Show("Pilih data yang akan di edit terlebih dahulu");
                return;
            }

            var contex = new InventoryEntity();
            var transaction = new transaction();
            transaction.type_id = Convert.ToInt32(txt_transaksi_type.Text);
            transaction.trans_code = txt_transaksi_code.Text;
            transaction.remarks = txt_transaksi_keterangan.Text;
            transaction.id = Convert.ToInt32(selected_transaction);
            contex.transactions.Add(transaction);

            //string query = "UPDATE DBO.'transaction SET code='" + txt_master_code.Text + "', name='" + txt_master_nama.Text + "', quantity_on_hand = " + txt_master_qty.Text + "', remarks='" + txt_master_keterangan.Text + " WHERE id=" + selected_master;
            //contex.Database.ExecuteSqlCommand(query);
            clear_type();
            refresh_type();
            MessageBox.Show("Update data berhasil!!");
        }

        private void table_transaksi_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DetaiTransaction detaiTransaction = new DetaiTransaction();
            detaiTransaction.setTransaction(selected_transaction);
            detaiTransaction.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.refresh_items();
            this.refresh_transaction();
            this.refresh_type();
        }
    }
}
